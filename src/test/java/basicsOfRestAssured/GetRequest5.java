package basicsOfRestAssured;

import java.util.Arrays;

import org.hamcrest.Matchers;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.ValidatableResponse;

public class GetRequest5 {
	
	@Test
	public void restAssuredGet() {

		RestAssured.get("https://restful-booker.herokuapp.com/booking/10").then().statusCode(Matchers.in(Arrays.asList(200, 201)))
		.body("firstname", Matchers.startsWithIgnoringCase("m"));
	}

}
