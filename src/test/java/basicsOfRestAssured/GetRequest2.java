package basicsOfRestAssured;

import org.hamcrest.Matchers;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;

public class GetRequest2 {
	
	@Test
	public void restAssuredGet() {

		
		  Response getResponse =
		  RestAssured.get("https://restful-booker.herokuapp.com/booking/10");
		  
		  //String firstName = getResponse.jsonPath().get("firstname").toString();
		  
		  //System.out.println(firstName);
		 
		
		Object obj = getResponse.jsonPath().get("firstname");
		
		if(obj instanceof String) {
			
			System.out.println(obj.toString());
		}else if(obj instanceof Integer){
			
			int i =(Integer)obj;
			
			System.out.println(i);
		}
		
	}


}
