package basicsOfRestAssured;

import org.junit.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class GetRequest {
	
	
	@Test
	public void restAssuredGet() {
		
		Response getResponse = RestAssured.get("https://restful-booker.herokuapp.com/booking/1");
		String body = getResponse.asString();
		System.out.println(body);
		
		int statcode = getResponse.getStatusCode();
		System.out.println(statcode);
		
		Assert.assertEquals(200, statcode);
		
	}

}
