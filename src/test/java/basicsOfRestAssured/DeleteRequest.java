package basicsOfRestAssured;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class DeleteRequest {
	
	@Test(priority =1)
	public void getToken(ITestContext dataManager) {
		
		String token = RestAssured
		.given()
			.log()
			.all()
			.contentType(ContentType.JSON)
			.baseUri("https://restful-booker.herokuapp.com/")
			.body("{\r\n" + 
					"    \"username\" : \"admin\",\r\n" + 
					"    \"password\" : \"password123\"\r\n" + 
					"}")
		.when().log().all().post("auth").then().extract().jsonPath().get("token");
		
		dataManager.setAttribute("token" , token);
	
		
	}
	
	@Test(priority =2)
	public void restAssuredDelete(ITestContext dataManager) {

		RestAssured.given().log().all().baseUri("https://restful-booker.herokuapp.com/").contentType(ContentType.JSON)
				.header("Cookie", "token="+ dataManager.getAttribute("token"))
				.pathParam("bookingid", 1)
		.when().put("booking/{bookingid}").then().log().all().statusCode(201);

}

}
