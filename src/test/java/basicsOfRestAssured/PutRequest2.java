package basicsOfRestAssured;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import com.sun.org.apache.bcel.internal.classfile.Method;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class PutRequest2 {

	
	@Test(priority =1)
	public void getToken(ITestContext dataManager, Method m) {
		
		String token = RestAssured
		.given()
			.log()
			.all()
			.contentType(ContentType.JSON)
			.baseUri("https://restful-booker.herokuapp.com/")
			.body("{\r\n" + 
					"    \"username\" : \"admin\",\r\n" + 
					"    \"password\" : \"password123\"\r\n" + 
					"}")
		.when().log().all().post("auth").then().extract().jsonPath().get("token");
		
		dataManager.setAttribute("token" + m.getName(), token);
		
		System.out.println((String)dataManager.getAttribute("tokengenerateToken"));
		
	}
	
	@Test(priority =2)
	public void restAssuredPut(ITestContext dataManager) {

		RestAssured.given().log().all().baseUri("https://restful-booker.herokuapp.com/").contentType(ContentType.JSON)
				.header("Cookie", "token="+ dataManager.getAttribute("token")).body("{\r\n" + 
						"    \"firstname\" : \"Arshita\",\r\n" + 
						"    \"lastname\" : \"Yadav\",\r\n" + 
						"    \"totalprice\" : 111,\r\n" + 
						"    \"depositpaid\" : true,\r\n" + 
						"    \"bookingdates\" : {\r\n" + 
						"        \"checkin\" : \"2018-01-01\",\r\n" + 
						"        \"checkout\" : \"2019-01-01\"\r\n" + 
						"    },\r\n" + 
						"    \"additionalneeds\" : \"Breakfast\"\r\n" + 
						"}")
		.when().put("booking/1").then().log().all().statusCode(200);

}
}
