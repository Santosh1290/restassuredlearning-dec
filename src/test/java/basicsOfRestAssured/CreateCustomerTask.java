package basicsOfRestAssured;

import org.hamcrest.Matchers;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class CreateCustomerTask {
	
	@Test
	public void restAssuredPost() {

		Response resp = RestAssured.given().log().all().baseUri("https://restful-booker.herokuapp.com/").contentType(ContentType.JSON).body("{\r\n" + 
						"    \"firstname\" : \"Sanjay\",\r\n" + 
						"    \"lastname\" : \"Yadav\",\r\n" + 
						"    \"totalprice\" : 111,\r\n" + 
						"    \"depositpaid\" : true,\r\n" + 
						"    \"bookingdates\" : {\r\n" + 
						"        \"checkin\" : \"2018-01-01\",\r\n" + 
						"        \"checkout\" : \"2019-01-01\"\r\n" + 
						"    },\r\n" + 
						"    \"additionalneeds\" : \"Breakfast\"\r\n" + 
						"}")
		.log().all().post("booking");
		
		resp.prettyPrint();
	
	}
}
