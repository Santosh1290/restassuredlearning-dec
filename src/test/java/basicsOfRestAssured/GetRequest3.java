package basicsOfRestAssured;

import org.hamcrest.Matchers;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

public class GetRequest3 {
	
	@Test
	public void restAssuredGet() {

		JsonPath jsonPath = RestAssured.get("https://restful-booker.herokuapp.com/booking/10").then().statusCode(200)
				.body("firstname", Matchers.equalTo("Susan"))
				.body("bookingdates.checkin", Matchers.equalTo("2015-10-13"))
				.extract().jsonPath();
		
		System.out.println(jsonPath.getString("firstname"));
		System.out.println(jsonPath.getString("bookingdates.checkin"));
		
		
	}

}
