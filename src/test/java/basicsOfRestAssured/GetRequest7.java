package basicsOfRestAssured;

import org.hamcrest.Matchers;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.ValidatableResponse;

public class GetRequest7 {
	@Test
	public void restAssuredGet() {

		int bookid = RestAssured.get("https://restful-booker.herokuapp.com/booking").jsonPath().getInt("[0].bookingid");
	    System.out.println(bookid);
		
	}
}
