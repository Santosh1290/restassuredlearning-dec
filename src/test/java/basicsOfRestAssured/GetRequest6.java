package basicsOfRestAssured;

import java.util.Arrays;

import org.hamcrest.Matchers;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class GetRequest6 {

	@Test
	public void restAssuredGet() {

		Response respo = RestAssured.get("https://restful-booker.herokuapp.com/booking/10");
		
		respo.prettyPrint();
	}
}
