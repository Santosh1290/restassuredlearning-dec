package basicsOfRestAssured;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class GetRequest8 {
	
	@Test
	public void restAssuredGet() {

		RestAssured
		.given()
		.when().get("https://restful-booker.herokuapp.com/booking/10")
		.then()
		.statusCode(200);
}
}
