package payloadHandling;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class PayloadAsMap {
	
	
	
	@Test
	public void PayloadasMap()
	{
		
		Map<String, Object> emp1 = new LinkedHashMap<>();
		
		Map<String, Object> addressMap = new LinkedHashMap<>();
		addressMap.put("street", "Vrindavan Garden");
		addressMap.put("city", "Sahibabad");
		
		ArrayList<Object> mob = new ArrayList<>();
		mob.add(789797);
		mob.add(12331);

		
		emp1.put("name", "santosh");
		emp1.put("sub", "test");
		emp1.put("address", addressMap);
		emp1.put("mobile", mob);
		
		Map<String, Object> emp2 = new LinkedHashMap<>();
		
		Map<String, Object> addressMap1 = new LinkedHashMap<>();
		addressMap1.put("street", "Shaimar Garden");
		addressMap1.put("city", "Ghaziabad");

		
		emp2.put("name", "aakarsh");
		emp2.put("sub", "testing");
		emp2.put("address", addressMap);
	
		List<Map<String, Object>> allemp = new ArrayList<>();
		allemp.add(emp1);
		allemp.add(emp2);
		
		
		RestAssured
			.given()
				.log()
				.all()
				.contentType(ContentType.JSON)
				.baseUri("https://restful-booker.herokuapp.com/")
				.body(allemp)
				.when()
				.post("booking")
			.then()
				.log()
				.all()
				.statusCode(200);
		
	}

}
