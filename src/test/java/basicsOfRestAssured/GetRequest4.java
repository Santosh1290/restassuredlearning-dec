package basicsOfRestAssured;

import org.hamcrest.Matchers;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Headers;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class GetRequest4 {

	@Test
	public void restAssuredGet() {

	     RestAssured.get("https://restful-booker.herokuapp.com/booking/10").then().time(Matchers.lessThan(5000L));
	     
	     Response resp = RestAssured.get("https://restful-booker.herokuapp.com/booking/10");
	     long l = resp.getTime();
	     
	     System.out.println(l);
	     
	     Headers r = resp.getHeaders();
	     r.get("Connection");
	     
	     System.out.println(r.get("Connection"));
	     

	}
}
