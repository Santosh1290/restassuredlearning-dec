package basicsOfRestAssured;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;

public class GetRequest1 {

	@Test
	public void restAssuredGet() {

		ValidatableResponse getResponse = RestAssured.get("https://restful-booker.herokuapp.com/booking/10").then().statusCode(200)
				.body("firstname", Matchers.equalTo("Sally"))
				.body("bookingdates.checkin", Matchers.equalTo("2020-03-26"));
		
	}

}
