package basicsOfRestAssured;

import org.hamcrest.Matchers;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

public class GetCustomerTask {
	
	@Test
	public void restAssuredGetCustomer() {
		
		
		 JsonPath respo =
		 RestAssured.get("https://restful-booker.herokuapp.com/booking/19").then().statusCode(200).body("firstname", Matchers.equalTo("Sanjay"))
		 .extract().jsonPath();
		 
		 System.out.println(respo.getString("firstname"));
	}

}
